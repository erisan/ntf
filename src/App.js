import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

import DashboardHome from "./pages/Dashboard/index"
import Login from "./pages/Auth/login"
import ConnectAccount from "./pages/Dashboard/connect-account";
import Offers from "./pages/Dashboard/offers";
import Proposal from "./pages/Dashboard/proposal";
import Tree from "./pages/Dashboard/tree";
import Owned from "./pages/Dashboard/owned";
import NewProposal from "./pages/Dashboard/new-proposal";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/login" element={<Login />} />
        <Route path="/" element={<DashboardHome />} />
        <Route path="/connect-account" element={<ConnectAccount />} />
        <Route path="/trees" element={<Tree />} />
        <Route path="/offers" element={<Offers />} />
        <Route path="/proposal" element={<Proposal />} />
        <Route path="/owned" element={<Owned />} />
        <Route path="/new-proposal" element={<NewProposal />} />
        
      </Routes>
    </BrowserRouter>

  )
}



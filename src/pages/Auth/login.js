import React, { useEffect, useRef, useState } from "react";
import useAuth from "../../context/authContext";
import { Navigate } from "react-router-dom";
import { toast } from "react-toastify";

export default function Login() {
  const emailRef = useRef();
  const passwordRef = useRef();

  const [agreedTerm, setAgreedTerm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [pageLoading, setPagelaoding] = useState(true)

  const {
    signInWithEmailAndPassword,
    setCurrentUser,
    currentUser,
    createUserWithEmailAndPassword,
  } = useAuth();

  const continueToSignup = async () => {
    setLoading(true);
    try {
      const newUser = await createUserWithEmailAndPassword(
        emailRef.current.value,
        passwordRef.current.value
      );
      setCurrentUser(newUser.user.providerData);
      toast.success("Login successfull.");
      setLoading(false);
    } catch (error) {
      toast.success("Error occured while trying to signup, try again.");
      setLoading(false);
    }
  };

  const handleSubmit = (e) => {
    setLoading(true);

    e.preventDefault();
    if (!agreedTerm) {
      toast.error("Accept terms to continue");
      return;
    }
    signInWithEmailAndPassword(
      emailRef.current.value,
      passwordRef.current.value
    )
      .then((res) => {
        setCurrentUser(res.user.providerData);
        toast.success("Login successfull.");
        setLoading(false);
      })
      .catch((err) => {
        console.log(err.message);
        setLoading(false);

        if (err.message.includes("auth/user-not-found")) {
          continueToSignup();
        }
      });
  };

  if(currentUser){
    setTimeout(()=> window.location.href = "/", 2000);
  }

  return (
    <form onSubmit={handleSubmit} className="container-fluid">
      <div className="col-xl-7 col-lg-11 col-md-12 mx-auto alignycenter mt-5">
        <div className="row">
          <div className="col-md alignycenter md-100vh">
            <div className="d-inline-block">
              <img src="/logo1.png" alt="" />
              <div className="font20 my-4">
                Sign-in to your Creator's Dashboard, if you don't have an
                account. Please download the app
              </div>
            </div>
          </div>

          <div className="col-md alignycenter">
            <div className="card-container">
              <h2 className="title mb-4">Creator Dashboard Sign-in</h2>
              <div></div>
              <div className="mb-3">
                <label htmlFor="email" className="form-label">
                  Email{" "}
                </label>
                <input
                  required
                  type="email"
                  ref={emailRef}
                  className="form-control input"
                  id="email"
                  aria-describedby="emailHelp"
                />
              </div>

              <div className="mb-3">
                <label htmlFor="password" className="form-label">
                  Passwords
                </label>
                <input
                  required
                  type="password"
                  ref={passwordRef}
                  className="form-control"
                  id="password"
                  aria-describedby="emailHelp"
                />
              </div>

              <div className="">
                <div className="mb-3 form-check d-flex gray-card position-relative overflow-hidden">
                  <div className="mx-4">
                    <input
                      type="checkbox"
                      onClick={() => setAgreedTerm(!agreedTerm)}
                      className="form-check-input mr-3"
                      id="terms mr-3"
                      style={{ width: "22px", height: "22px" }}
                    />
                  </div>

                  <label className="form-check-label" htmlFor="terms">
                    By creating an account you agree to our Terms of Use
                  </label>
                </div>
              </div>

              <button type="submit" className="btn button-pink">
                {" "}
                {loading ? "Authenticating ..." : "Login"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

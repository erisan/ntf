import React, { useRef } from "react";
import useAuth from "../../context/authContext";
import { ToastContainer, toast } from "react-toastify";

export default function Login() {
  function validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const emailRef = useRef();
  const passwordRef = useRef();

  const { signInWithEmailAndPassword } = useAuth();

  const handleSubmit = (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(
      emailRef.current.value,
      passwordRef.current.value
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <form onSubmit={handleSubmit} className="container-fluid">
      <div className="col-md-7 mx-auto alignycenter">
        <div className="row">
          <div className="col-md alignycenter" style={{ height: "100vh" }}>
            <div className="d-inline-block">
              <img src="/logo1.png" />
              <div className="font20 mt-4">
                Sign-in to your Creator's Dashboard, if you don't have an
                account. Please download the app
              </div>
            </div>
          </div>

          <div className="col-md alignycenter">
            <div className="card-container">
              <h2 className="title mb-4">Creator Dashboard Sign-in</h2>
              <div></div>
              <div className="mb-3">
                <label htmlFor="email" className="form-label">
                  Email{" "}
                </label>
                <input
                  required
                  type="email"
                  ref={emailRef}
                  className="form-control input"
                  id="email"
                  aria-describedby="emailHelp"
                />
              </div>

              <div className="mb-3">
                <label htmlFor="password" className="form-label">
                  Passwords
                </label>
                <input
                  required
                  type="password"
                  ref={passwordRef}
                  className="form-control"
                  id="password"
                  aria-describedby="emailHelp"
                />
              </div>

              <div className="gray-card">
                <div className="mb-3 form-check">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="terms"
                  />
                  <label className="form-check-label" htmlFor="terms">
                    By creating an account you agree to our Terms of Use
                  </label>
                </div>
              </div>

              <button type="submit" className="btn button-pink">
                {" "}
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

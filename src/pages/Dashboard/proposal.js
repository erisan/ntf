import React from "react";
import Layout from "../../layout";

export default function Proposal() {

    return (
        <Layout hideRightNav={true} title="Creator dashboard">
            <div className="form-input">
                <div className="overflow-hidden my-5 clearfix position-relative"></div>
                <div className="overflow-hidden my-5 clearfix position-relative"></div>

                <h1 className="font40 color-black">Smart Contract Back-end Infrastucture Integration Harness</h1>

                <div className="font25 col-xl-6 col-lg-9 col-md-10">These functions will be called by Livetree Server Side C# or the Dashboard from the creator or the ERC20 holder</div>
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-5 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            1. RoyaltyItemFactoryProxy - Mint()
                        </div>
                        <div className="col-md-6 font20" style={{ color: "#4A4A4A" }}>
                            https://github.com/fractional-company/contracts/blob/master/src/ERC721VaultFactory.sol
                        </div>
                    </div>
                    {/*  */}
                    <div className="row mt-5">
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label > ERC20 Token Symbol</label>
                                <input class="form-control" />
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label > Total Supply</label>
                                <input value="Default 100m" class="form-control" />
                            </div>
                        </div>

                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label > ERC20 Token Logo</label>
                                <input value="" class="form-control" />
                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-3">
                            <div class="form-group">
                                <label > ERC20 Token Name</label>
                                <input value="" class="form-control" />
                            </div>
                        </div>
                    </div>
                    {/*  */}
                    <div className="row  mt-4">
                        <div className="col-xl col-lg-4">
                            <div class="form-group">
                                <label > NFT Media URL</label>
                                <input class="form-control" />
                            </div>
                        </div>
                        <div className="col-xl col-lg-4">
                            <div class="form-group">
                                <label > NFT Metadata JSON URL</label>
                                <input value="" class="form-control" />
                            </div>
                        </div>

                        <div className="col-xl col-lg-4">
                            <div class="form-group">
                                <label > NFT ViewStats JSON URL</label>
                                <input value="" class="form-control" />
                            </div>
                        </div>

                        <div className="col-xl col-lg-4">
                            <div class="form-group">
                                <label >NFT Picture URL</label>
                                <input value="" class="form-control" />
                            </div>
                        </div>

                        <div className="col-xl col-lg-4">
                            <div class="form-group">
                                <label >NFT App Link URLL</label>
                                <input value="" class="form-control" />
                            </div>
                        </div>
                        <div className="col-xl col-lg-4">
                            <div class="form-group">
                                <label >ItemId</label>
                                <input value="" class="form-control" />

                            </div>
                        </div>
                        <div className="col-xl-1">
                            <span className="ml-4">OR</span>

                        </div>


                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label >@param _token the ERC721 token addresses []</label>
                                <input value="" class="form-control" />

                            </div>
                        </div>

                    </div>
                    {/*  */}
                    <div className="row mt-4">
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label >Owner</label>
                                <input value="address of owner" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-3">
                            <div class="form-group">
                                <label >Owner Initial Percentage of tokens</label>
                                <input value="" class="form-control" />

                            </div>
                        </div>

                    </div>
                    {/*  */}
                    {/*  */}
                    <div className="row mt-4">
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label >Livetree</label>
                                <input value="address of livetree" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-3">
                            <div class="form-group">
                                <label>Livetree Initial Percentage of tokens</label>
                                <input value="default to 50%" class="form-control" />

                            </div>
                        </div>

                    </div>

                    {/*  */}

                    <div className="row mt-4">
                        <div className="col-xl-4 col-lg-5">
                            <div class="form-group">
                                <label >Buyer</label>
                                <input placeholder="address of livetree" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-4 col-lg-5">
                            <div class="form-group">
                                <label>Buyer Initial Percentage of tokens</label>
                                <input value="default to 50%" class="form-control" />

                            </div>
                        </div>

                    </div>
                    {/*  */}

                    {/*  */}
                    <div className="row mt-4">
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label >EscrowManager</label>
                                <input placeholder="address of escrowmanger" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-4 col-lg-5">
                            <div class="form-group">
                                <label>EscrowManager Initial Percentage of tokens</label>
                                <input class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label>BranchUsernameOfBuyer</label>
                                <input class="form-control" />

                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label>ItemId</label>
                                <input class="form-control" />

                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-3">
                            <div class="form-group">
                                <label>offerId</label>
                                <input class="form-control" />

                            </div>
                        </div>

                        <button className="button-harsh btn mt-5">Mint NFT Governor</button>
                    </div>
                    {/*  */}
                </form>

                {/*  */}

                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            2. EscrowManager - ReleaseDistributeERC20()
                        </div>


                    </div>


                    <div className="row mt-4">
                        <div className="col-xl-4 col-lg-5 col-6">
                            <div class="form-group">
                                <label >addressToSendTo</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-4 col-lg-5 col-6">
                            <div class="form-group">
                                <label>offerId</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>
                        <br />
                        <button className="btn button-harsh mt-5">ReleaseDistribute()</button>
                    </div>


                </form>
                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            3. EscrowManager - DepositEscrow()
                        </div>

                        <span>called by mint</span>


                    </div>


                    <div className="row mt-4">
                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label >erc20 token address</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label>erc20</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>
                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label>itemid</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>
                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label>offerid</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>
                        <br />
                        <button className="btn button-harsh mt-5">DepositEscrow()</button>
                    </div>


                </form>
                {/*  */}


                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            4. RoyaltyItemTreasuryGovernor - AcceptOffer()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label >eInput the ERC20 TreeDollar (TRED)</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label>% of the ERC20 (e.g. 10% then 10,000,000)</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>
                        <div className="col-xl-3 col-lg col-6">
                            <div class="form-group">
                                <label>Returns ERC20 Actual Tokens</label>
                            </div>
                        </div>

                        <br /> </div>
                    <button className="btn button-harsh mt-5">AcceptOffer()</button>



                </form>

                {/*  */}
                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            5. LivetreeRoyaltyManager - PayRoyaltiesToRoyaltyItemTreasureyGovernorContracts()
                        </div>
                    </div>

                    <div className="row mt-4">
                        <div className="col-xl-4 col-lg-5 col-6">
                            <div class="form-group">
                                <label >ERC20 TreeDollar (TRED)amounts[]</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-5 col-lg-5 col-6">
                            <div class="form-group">
                                <label>Deposit into RoyaltItemTreasuryGovernor Addresses[]</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>
                        <br />   
                        </div>

                    <button className="btn button-harsh mt-5">PayRoyaltyToItem()</button>
                </form>

                {/*  */}

                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            6. RoyaltyItemTreasuryGovernor - DepositRoyalty()
                        </div>
                    </div>

                    <div className="row mt-4">
                        <div className="col-xl-3 col-lg-4 col-6">
                            <div class="form-group">
                                <label >ERC20 TreeDollar (TRED)</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-4 col-6">
                            <div class="form-group">
                                <label>The function is called by 5</label>
                                <input placeholder="" class="form-control" />

                            </div>
                        </div>


                        <br /> </div>

                    <button className="btn button-harsh mt-5">DepositRoyalty()</button>


                </form>

                {/*  */}

                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            7. RoyaltyItemTreasuryGovernor - CashOutBurnERC20ThenReturnTrees()
                        </div>
                    </div>


                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label >Input ERC20 (e.g. DUDE token - burn them)</label>
                            </div>

                            <div class="form-group">
                                <label >Returns ERC20 TreeDollar (TRED)</label>
                            </div>
                        </div>


                        <br /> </div>
                    <button className="btn button-harsh mt-5">CashOutExchangeForTreesBurnERC20SharePercentage()</button>



                </form>
                {/*  */}

                {/*  */}

                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            8. LivetreeRoyaltyManager - SetTreeToUnderlyingExchangeRate()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label>ERC20 TreeDollar (TRED)</label>
                                <label>1</label>
                            </div>
                            <div class="form-group">
                                <label >Exchange Rate)</label>
                                <input placeholder="" class="form-control" />
                            </div>
                        </div>
                        <br />   </div>
                    <button className="btn button-harsh mt-5">
                        SetTreeToUnderlyingExchangeRate()
                    </button>

                </form>
                {/*  */}

                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            9. LivetreeRoyaltyManager - ExchangeTrees()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label>Input ERC20 TreeDollar (TRED)</label>
                            </div>
                            <div class="form-group">
                                <label >Returns ERC20 (USDT)</label>
                            </div>
                        </div>
                        <br />  </div>
                    <button className="btn button-harsh mt-5">
                        ExchangeTrees()
                    </button>

                </form>
                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            10. LivetreeRoyaltyManager - DepositUnderlyingToken()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label>Input ERC20 underlying (e.g. stable coin like Tether)</label>
                            </div>
                        </div>
                        <br /> </div>
                    <button className="btn button-harsh mt-5">
                        ExchangeTrees()
                    </button>

                </form>
                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            11. RoyaltyItemTreasuryGovernor - ERC20SharesTotalSupply()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label>Returns ERC20 Total supply</label>
                            </div>
                        </div>
                        <br /> </div>
                    <button className="btn button-harsh mt-5">
                        ERC20SharesTotalSupply()
                    </button>

                </form>
                {/*  */}

                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            12. RoyaltyItemTreasuryGovernor - ListNFTContractAddresses()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label>Returns ERC741 addresses</label>
                            </div>
                        </div>
                        <br />  </div>
                    <button className="btn button-harsh mt-5">
                        ListNFTContractAddresses()
                    </button>

                </form>


                {/*  */}
                <form className="container-fluid mt-5">
                    {/*  */}
                    <div className="row">
                        <div className="col-md-12 font25 fw-bold" style={{ color: "#4A4A4A", fontWeight: "bold" }}>
                            13. RoyaltyItemTreasuryGovernor - GetRoyaltyAmountAvailable()
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div class="form-group">
                                <label>Returns ERC20 Trees in the contract (e.g. 40.1)</label>
                            </div>
                        </div>
                        <br />   </div>
                    <button className="btn button-harsh mt-5">
                        ERC20SharesTotalSupply()
                    </button>

                </form>
            </div>
        </Layout>
    );
}

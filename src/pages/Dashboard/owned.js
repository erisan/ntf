import React, { useEffect, useState } from 'react'
import { Navigate } from 'react-router';
import { toast } from 'react-toastify';
import useAuth from '../../context/authContext';
import useMetaMask from '../../context/metaMaskContext';
import Layout from "../../layout";
import { itemObject } from '../../services/data/dbCollections';
import { supportWallets } from '../../services/metamask/actions';
import { addNewBuyInOffer } from '../../services/offers';


export default function Owned() {

    const { currentUser, userProfile } = useAuth();
    const { walletAccountInfo } = useMetaMask();
    const [percent, setPercent] = useState(10);
    const [buyOffer, setBuyOffer] = useState(0);
    const [expireTime, setExpireTime] = useState();
    const [doneAddingItem, setDoneAddingItem] = useState(false);
    const [activeWallet, setActiveWallet] = useState({});
    const [formIsNotReady, setFormIsNotReady] = useState(true)
    const [offerError, setOfferError] = useState(false)

    const { blockchainNetwork, blockchainNetworkId } = walletAccountInfo;

    useEffect(() => {
        setActiveWallet(supportWallets[blockchainNetworkId]);
    }, [blockchainNetworkId]);

    console.log(userProfile, '....', walletAccountInfo)

    const resolveSupportActiveWallet = (chainId) => {
        return Object.values(supportWallets).map(wallet => (
            <a type="button" className={`d-inline-block ml-2 font20 p-1 ${wallet.chainId == chainId ? 'btn btn-secondary' : ''}`} style={{ fontSize: 15 }}> {wallet.label}  </a>
        ))
    }

    const calulateBuyInPercentageValue = (activeWallet) => {
        if (activeWallet?.value) {
            return parseFloat(buyOffer * activeWallet.value).toLocaleString()
        } else return '0.00'
    }

    const handleMakeBuyInOffer = async (e) => {
        e.preventDefault()

        if (!percent || !buyOffer || !expireTime) {
            toast.error("all fields are required !!!")
        } else {

            await addNewBuyInOffer({
                blockchainNetwork,
                blockchainNetworkId,
                buyerWalletAddress: walletAccountInfo.address,
                itemOwnerBranchUsername: userProfile.BranchUsername,
                buyerBranchUserName: userProfile.BranchUsername,
                buyerTreeOfferAmount: buyOffer,
                buyerPercentageBuyIn: percent,
                buyerProfilePic: userProfile.PictureUrl,
                buyerOfferExpiresDate: expireTime,
                itemPicURL: itemObject.mediaURL,
                itemTitle: itemObject.itemTitle,
                itemId: itemObject.itemId,
            });
            // redirect to offers
            setDoneAddingItem(true)
        }

    }

    if (doneAddingItem) {
        return <Navigate to="/offers" />;
    }

    return (
        <Layout showSubHeader>
            <div className="container-fluid">
                <div className="row gap-5">
                    <div className="col-xl-8 card-container mt-4 py-5 font-16 color-black">
                        <div className="font20 color-black mb-4">Your current stake</div>
                        <div className="row py-3">
                            <div className="col-md-4">
                                Your Royalty %
                            </div>
                            <div className="col-md">
                                10%
                            </div>
                        </div>
                        <div className="row py-3">
                            <div className="col-md-4">
                                Your Royalty ERC20 Tokens
                            </div>
                            <div className="col-md d-flex">
                                <div className="mr-3">
                                    <img style={{ width: "30px", height: "auto" }} src="/dudeCoin.png" />
                                </div>
                                <div className="mr-3">
                                    DUDE <br />Dude Token
                                </div>
                                <div>
                                    <div>10,000,000</div>
                                    <div>of 100,000,000 total supply</div>
                                </div>

                            </div>

                        </div>

                        <div className="row py-3">
                            <div className="col-md-4">
                                Underlying NFT Addresses
                            </div>

                            <div className="col-md">
                                <div className="d-flex">
                                    <div className="mr-3">0x44f997e600085956d46629c7f7</div>
                                    <u><a style={{ color: "#0022FF" }}>View in block explorer</a></u>
                                </div>
                                <div className="d-flex">
                                    <div className="mr-3">0x44f997e600085956d46629c7f7</div>
                                    <u><a style={{ color: "#0022FF" }}>View in block explorer</a></u>
                                </div>
                                <div className="d-flex">
                                    <div className="mr-3">0x44f997e600085956d46629c7f7</div>
                                    <u><a style={{ color: "#0022FF" }}>View in block explorer</a></u>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="col-1  dd-md-none" style={{ maxWidth: "2%" }}>
                    </div>

                    <div className="col-lg card-container mt-4 font-16 color-black">
                        <div className="font20 color-black mb-4">Make Buy-In Offer</div>
                        <form onSubmit={handleMakeBuyInOffer}>
                            <div class="form-group">
                                <label > Buy-in Percentage</label>
                                <input min="1" value={percent} type="number" name="percentage" class="form-control text-center"
                                    onChange={(e) => {
                                        setPercent(e.target.value)
                                    }}
                                />
                                {/* <div className="mt-3" style={{ borderRadius: 20, backgroundColor: "#0046FF" }}>
                                    <input className="w-100" id="range" min={40} onChange={(e) => {
                                        setPercent(e.target.value)
                                    }} maxLength={100} type="range" />
                                </div> */}
                                {/* <div className="text-center"><small > ≈ $ {calulateBuyInPercentageValue(activeWallet)} </small></div> */}
                            </div>

                            <div class="form-group mt-3">
                                <label > Your Buy-In Offer</label>
                                <input style={{ border: offerError ? "solid 1px red": "solid opx"}} value={buyOffer} placeholder="1000" onChange={(e) => {
                                    const num = e.target.value;
                                    console.log(num, walletAccountInfo?.balance)
                                    if (!isNaN(num)) {
                                        // if (walletAccountInfo && walletAccountInfo?.balance < num) {
                                        //     toast.error("insuficient balance");
                                        //     setFormIsNotReady(true);
                                        //     setOfferError(true)
                                        // } else {
                                            setFormIsNotReady(false);
                                        //     setOfferError(false);
                                        // }
                                        setBuyOffer(num);
                                    }
                                }} class="form-control text-center" />
                            </div>
                            <div className="text-center"><small > ≈ $ {calulateBuyInPercentageValue(activeWallet)} </small></div>
                            <div class="form-group mt-3">
                                <label > Your Offer Expires</label>
                                <input type="date" value={expireTime} onChange={(e) => {
                                    const num = e.target.value;
                                    setExpireTime(num)
                                }} class="form-control text-center" />
                            </div>
                            <div style={{ marginBottom: '20px' }}>
                                {resolveSupportActiveWallet(blockchainNetworkId)}
                            </div>
                            <button type="submit" style={{ cursor: formIsNotReady || offerError ? 'not-allowed': 'pointer' }} disabled={formIsNotReady || offerError} className="btn button-pink" >Buy In</button>
                        </form>
                    </div>

                </div>

            </div>

        </Layout>
    )
}

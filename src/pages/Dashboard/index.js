import React from "react";
import { Link, NavLink } from "react-router-dom";
import useAuth from "../../context/authContext";
import Layout from "../../layout";
import { addUserProfile, updateUserBalance } from "../../services/firebase/actions";


export default function Index() {

  const { userBalance, currentUser } = useAuth();

  const handleUpdateUserBalance = async () => {
    if (currentUser) {
      await updateUserBalance(currentUser.uid, { WithdrawableTrees: Math.floor(Math.random() * 1000) })
    }
  }


  return (
    <Layout>
      {/* <NavLink to="/new-proposal">HELLO</NavLink> */}
      <div className="col-12">
        <div className="overflow-hidden my-xl-5"></div>
        <div className="row w-100 margin" style={{ margin: "0px" }}>
          <div className="col-xl col-lg-6 mt-5 col-md-6 ">
            <div className="card-container position-relative overflow-hidden ">
              <div className="font20 mb-3">Income</div>

              <div className="alignycenter">
                <span>
                  <img src="/flower1.png" alt="" />
                </span> &nbsp; &nbsp;
                <span className="bigTitle mr-3"> {parseInt(userBalance?.WithdrawableTrees).toLocaleString()} </span>
                <span className="font16">trees earned</span>
              </div>

              <div class="col-lg-10 mx-auto mb-3">
                <button className="w-100 mt-3 button-white">
                  Exchange for TREE coins
                </button>
                <button className="w-100 mt-3 button-white">
                  Exchange Rewards
                </button>
                <button
                  className="w-100 mt-3 button-orangeGradient"
                  onClick={(e) => {
                    if (currentUser) {
                      addUserProfile(currentUser.uid)
                      console.log(currentUser.uid, 'uiiii')
                    }
                  }}
                >
                  Exchange Diamond Seed
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl col-lg-6 mt-5 col-md-6 ">
            <div className="card-container h-100 position-relative overflow-hidden ">
              <div className="font20 mb-3">Your posts converted to NFTs</div>

              <ul className="list-common list-unstyled">
                <li>Decision Proposals (10)</li>
                <li>
                  <Link to="/connect-account">Buy-in Offers (2)</Link>
                </li>
                <li>Create NFT from your post</li>
                <li>View your NFTs</li>
              </ul>
            </div>
          </div>
          <div className="col-xl col-lg-6 mt-5 col-md-6 ">
            <div className="card-container h-100 position-relative overflow-hidden ">
              <div className="font20 mb-3">
                You decide how Livetree operates
              </div>

              <ul className="list-common list-unstyled">
                <li>Decision Proposals (10)</li>
                <li>Upgrade SED to RSED</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

import React, { useEffect } from "react";
import { Navigate } from "react-router-dom";
import { connectMetaMask } from "../../services/metamask/actions";
import useAuth from "../../context/authContext";
import useMetaMask from "../../context/metaMaskContext";


export default function ConnectAccount() {

  const { currentUser } = useAuth();
  const { walletAddress } = useMetaMask();

  let connectedAddress;
  if (!walletAddress) {
    if (window.ethereum) {
      connectedAddress = window.ethereum.selectedAddress
    }
  }

  console.log(walletAddress, 'account object', connectedAddress)

  if (!currentUser) {
    return <Navigate to="/login" />;
  }

  if (walletAddress || connectedAddress) {
    return <Navigate to="/offers" />;
  }

  return (
    <div className="container-fluid">
      <div className="w-100 alignXandYcenter vh-100">
        <div className="col-xl-3 col-lg-6 col-md-8  text-center ">
          <div className="card-container position-relative overflow-hidden smallPadding">
            <div className="overflow-hidden my-5"></div>

            <img src="/matamask.png" className="img-fluid mx-auto" alt="" />
            <div className="overflow-hidden my-3"></div>

            <div className="font20 mb-3 text-center">
              Connect your Metamask Wallet
            </div>
            <div className="overflow-hidden my-4"></div>

            <div class="col-10  mx-auto mb-3">
              <button className="w-100 mt-3 button-orangeGradient text-center" onClick={() => connectMetaMask()}>
                Connect Metamask
              </button>
            </div>
            <div className="overflow-hidden my-5"></div>
          </div>
        </div>
      </div>
    </div>
  );
}

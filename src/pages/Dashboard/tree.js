import React from "react";
import Layout from "../../layout";

export default function Tree() {
  const [showPop, setShowPop] = React.useState(false);

  const tableData = [
    {
      name: "AshUniswap",
      percent: "10%",
      description:
        "Your DUDE tokens will be destroyed and exchanged for TREE ERC20 Tokens you can then exchange TREES for cUSD $3,240.00",
    },
    {
      name: "AshUniswap",
      percent: null,
      description: "You have 1,000 TREESexchange for cUSD,MOVR $3,240",
    },
  ];

  return (
    <Layout showSubHeader title="Creator dashboard">
      <div className="overflow-hidden my-5 clearfix position-relative"></div>
      <div className="overflow-hidden my-5 clearfix position-relative"></div>
      <div className="conatiner-fluid">
        <div className="w-100 position-relative  table150 table-responsive">
          <table class="table ourTable  ttt">
            {/* <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Handle</th>
                  </tr>
                </thead> */}
            <tbody>
              {tableData.map((data, index) => (
                <tr className="position-relative overflow-hidden" key={index}>
                  <td style={{ verticalAlign: "middle" }}>
                    <div>
                      {data.name} {data.timeRemainig}
                    </div>
                  </td>
                  {data.percent && (
                    <td style={{ verticalAlign: "middle" }}>
                      <div>
                        <img src="/dudeCoin.png" alt="" />
                        <div className="tiny-font">
                          DUDE <br />
                          Dude Token
                        </div>
                      </div>
                    </td>
                  )}
                  <td
                    colSpan={!data.percent ? 2 : null}
                    className={data.percent ? "reduce-width" : null}
                    style={{ verticalAlign: "middle" }}
                  >
                    <div className={!data.percent ? "tableB" : ""}>
                      <span> {data.description}</span>
                    </div>
                  </td>
                  {/* <td style={{ verticalAlign: "middle" }}>
                    <div>
                      <img src="/dudeCoin.png" alt="" />
                      <div className="tiny-font">
                        DUDE <br />
                        Dude Token
                      </div>
                    </div>
                  </td> */}

                  <td style={{ verticalAlign: "middle" }}>
                    <div className="position-relative">
                      <div className="w-100 text-center">
                        <button className="button mx-auto noWrap  d-inline-block w-75  button-orangeGradient">
                          Accept
                        </button>
                        <br />
                      </div>
                    </div>
                    <div></div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
}

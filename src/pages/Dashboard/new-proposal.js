import React from 'react'
import Layout from "../../layout";


export default function NewProposal() {

    const data = [
        {
            name: "Fix COMP Accrual Bug",
            status: "Executed",
            votes: "1.04M",
            vote_against: "0",
            total_votes: "1.04 M",
            total_addressess: "27 address"
        },
        {
            name: "Fix COMP Accrual Bug",
            status: "Executed",
            votes: "1.04M",
            vote_against: "0",
            total_votes: "555.66k",
            total_addressess: "27 address"
        }
        ,
        {
            name: "Fix COMP Accrual Bug",
            status: "Executed",
            votes: "1.04M",
            vote_against: "0",
            total_votes: "1.04",
            total_addressess: "27 address"
        },
        {
            name: "Fix COMP Accrual Bug",
            status: "Executed",
            votes: "1.04M",
            vote_against: "0",
            total_votes: "1.04",
            total_addressess: "27 address"
        }
    ]


    return (
        <Layout showSubHeader>
            <div className="container-fluid">
                <div className="text-right d-flex align-middle" style={{ alignItems: "center", justifyContent: "flex-end" }}>
                    <div className=" text-right font20 mr-4 ">
                        Add new proposal
                    </div>
                    <button className=" text-right button-purple px-5">
                        Delegate vote
 </button>
                </div>

                <div>
                    <div className="font30 mt-4" style={{ color: "#000" }}>Administration Panel</div>

                    <div className="col-lg-6 mt-4  font20">When proposal are ready to be queued into the timelock, or need to e cancelled because
                    of a drop below the support sharehold they will be listed later </div>
                    <hr />
                    <div className="w-100 overflow-hidden">
                        <div className="font20 float-left">Proposal</div>
                        <div className="font20 flaot-right">Actions</div>
                    </div>

                    <div className="clearfix mt-3"></div>

                    <div className="mb-3 text-center form-check d-flex gray-card position-relative overflow-hidden">
                        No Proposal found
                </div>


                </div>



                <div>
                    <hr />
                    <div class="font30" style={{ color: "rgb(0, 0, 0)" }}>Proposal</div>

                    <div className="table-responsive my-5">
                        <table class="table our-table">
                            <thead>
                                <tr>
                                    <th scope="col">Proposal</th>
                                    <th scope="col">Vote for</th>
                                    <th scope="col">Vote against</th>
                                    <th scope="col">Total votes</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data.map((res, index) => {
                                        return (
                                            <tr>
                                                <td>

                                                    <div className="d-flex">
                                                        <div className="mr-3">
                                                            <img style={{ width: "40px", height: "40px" }} src="/dudeCoin.png" />
                                                        </div>
                                                        <div>
                                                            <div>{res.name}</div>
                                                            <div className={res.status.toLowerCase()}>{res.status}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>

                                                    <div>
                                                        <div
                                                            style=
                                                            {{ color: "green" }}
                                                        >{res.total_votes}</div>
                                                        <div class="progress" style={{ height: "5px" }}>
                                                            <div
                                                                class="progress-bar w-75"
                                                                role="progressbar"
                                                                aria-valuenow="75"
                                                                aria-valuemin="0"
                                                                style=
                                                                {{ backgroundColor: "green" }}
                                                                aria-valuemax="100"
                                                            ></div>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td>

                                                    <div>
                                                        <div
                                                            style={{ color: "#d65163" }}
                                                        >{res.vote_against}</div>
                                                        <div class="progress" style={{ height: "5px" }}>
                                                            <div
                                                                class="progress-bar w-75"
                                                                role="progressbar"
                                                                aria-valuenow="75"
                                                                aria-valuemin="0"
                                                                aria-valuemax="100"
                                                                style={{ backgroundColor: "#d65163" }}
                                                            ></div>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td>

                                                    <div>
                                                        <div>{res.total_votes}</div>
                                                        <div>{res.total_addressess}</div>

                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>



                </div>

                <div className="row mt-lg-0 mt-5">
                    <div className="col-lg-6 font20" style={{ color: "#000" }}>
                        This is straight from
                         <div className="overflow-hidden mb-3"></div>
 Tally can we somehow just take their interface
                         <div className="overflow-hidden mb-3"></div>

  Here is the link
                         <div className="overflow-hidden mb-3"></div>

   https://www.withtally.com/governance/compound
                         <div className="overflow-hidden mb-3"></div>
    https://github.com/withtally/Tutorial-Deploy-Governance
                         <div className="overflow-hidden mb-3"></div>
     I have contacted them and asked if we can use it...
                    </div>

                    <div className="col-lg-6 mt-lg-0 mt-5">
                        <div style={{fontWeight: "bold"}} className="font20">Add a new proposal to compound</div>

                        <div class="form-group mt-3">
                            <label> Proposal title</label>
                            <input style={{height: "40px"}} type="text" placeholder="title of the proposal" class="form-control" />
                        </div>

                        <div class="form-group mt-3">
                            <label> Proposal title</label>
                            <br />
                            <textarea style={{height: "100px"}} className="w-100"> </textarea>
                        </div>

                        <div class="form-group mt-3 d-flex">
                            <div>
                                <ul>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                </ul>
                            </div>

                            <div>

                            </div>
                        </div>


                        <div class="form-group mt-3 d-flex">
                            <button className="w-100 button-purple">Create proposal</button>
                            </div>
                    </div>
                </div>
            </div>

        </Layout>
    )
}

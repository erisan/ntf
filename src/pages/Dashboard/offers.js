import { onSnapshot } from "@firebase/firestore";
import React, { useEffect, useState } from "react";
import useAuth from "../../context/authContext";
import Layout from "../../layout";
import { itemObject } from "../../services/data/dbCollections";
import { addNewBuyInCounterOffer } from "../../services/firebase/actions";
import { buyInOfferCollection, updateBuyInOffer } from "../../services/offers";

const Popup = ({ buyInOfferData, userId, onClose, userProfile }) => {
  const [proposalData, setProposalData] = useState({});

  const handleCounterProposal = async (e) => {
    e.preventDefault();

    await addNewBuyInCounterOffer({
      ...proposalData,
      offerId: buyInOfferData.buyInOfferId,
      creatorBackerUserId: userId,
      creatorBackerBranchusernameLower: userProfile.BranchUsername,
      creatorBackerProfilePic: userProfile.PictureUrl,
      buyerBranchUserNameLower: buyInOfferData.buyerUserName,
      dateTimeCounterOfferExpires: buyInOfferData.buyerOfferExpiresDate,
      itemId: itemObject.itemId,
    });

    onClose();
  };

  const handleClose = (e) => {
    if (e?.target?.classList[0]?.includes("popup-container")) {
      onClose();
    }
  };
  return (
    <div className="popup-container" onClick={handleClose}>
      <div
        className="col-md-4 overflow-hidden position-absolute"
        style={{
          minWidth: "300px",
          left: "50%",
          top: "50%",
          transform: "translate(-50% , -50%)",
        }}
      >
        <form className="row form-input" onSubmit={handleCounterProposal}>
          <div className="card-container mx-auto">
            <h4>Counter Proposal</h4>
            <div className="col-12 mt-4">
              <div className="">
                <div class="form-group">
                  <label>Percentage of tokens</label>
                  <input
                    onChange={(e) =>
                      setProposalData({
                        ...proposalData,
                        counterOfferPercentage: e.target.value,
                      })
                    }
                    placeholder="E.g 10%"
                    class="form-control"
                    type="number"
                    name="counterOfferPercentage"
                    required
                  />
                </div>
              </div>
            </div>
            <div className="col-12 mt-4">
              <div className="">
                <div class="form-group">
                  <label>Number of Diamond Seed</label>
                  <input
                    onChange={(e) =>
                      setProposalData({
                        ...proposalData,
                        counterOfferAmount: e.target.value,
                      })
                    }
                    placeholder="E.g 100"
                    class="form-control"
                    type="number"
                    name="counterOfferAmount"
                    required
                  />
                </div>
              </div>
            </div>
            <button className="button-orangeGradient">
              Make counter proposal
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default function Dashboard() {
  
  const { currentUser, userProfile } = useAuth();
  const [buyInOfferData, setBuyInOfferData] = useState({});

  useEffect(() => {
    async function loadBuyInoffer() {
      const unsubscribe = onSnapshot(
        buyInOfferCollection(),
        (querySnapshot) => {
          const cities = [];
          querySnapshot.forEach((doc) => {
            cities.push(doc.data());
          });
          setTableData(cities.length > 0 ? cities : []);
        }
      );
    }
    loadBuyInoffer();
  }, []);

  const [showPop, setShowPop] = useState(false);
  const [tableData, setTableData] = useState([]);

  console.log(tableData[0], "datab,,,,,,,,,,,,");

  const handleAcceptBuyOffer = (uid) => {
    updateBuyInOffer(uid, {
      dateOfferAccepted: new Date(),
    });
  };

  const closeProposalModal = () => {
    setShowPop(false);
  };

  function runTimmer(d) {
    var countDownDate = new Date(d).getTime();
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    return {
      days,
      hours,
      minutes,
      seconds,
    };
  }

  const getRemainingTime = (buyerOfferExpiresDate, dateOfferAccepted) => {
    if (
      buyerOfferExpiresDate?.seconds * 1000 >= Date.now() &&
      !dateOfferAccepted
    ) {
      const { days, hours, minutes, seconds } = runTimmer(
        buyerOfferExpiresDate.seconds * 1000
      );
      return (
        <span style={{ color: "#00cdc9" }}>
          {" "}
          {days} day{days > 1 && "s"} {hours} hr remaining{" "}
        </span>
      );
    } else {
      return null;
    }
  };

  return (
    <>
      <Layout current={2} showSubHeader title="Creator dashboard">
        <div className="overflow-hidden my-5 clearfix position-relative"></div>
        <div className="overflow-hidden my-5 clearfix position-relative"></div>
        <div className="conatiner-fluid">
          <div className="w-100 position-relative table-responsive">
            <table class="table ourTable">
              <tbody>
                {tableData.length > 0 ? (
                  tableData
                    .filter((data) => data.buyerPercentageBuyIn)
                    .map((data, index) => (
                      <tr
                        className="position-relative overflow-hidden"
                        key={index}
                      >
                        <td style={{ verticalAlign: "middle" }}>
                          <div>
                            {data.itemOwnerBranchUsername}
                            &nbsp; &nbsp;
                            <div>
                              {/* show the remaining day and hour of buyOff yet to Expire and accepted */}
                              {getRemainingTime(
                                data.buyerOfferExpiresDate,
                                data.dateOfferAccepted
                              )}
                            </div>
                          </div>
                        </td>
                        <td style={{ verticalAlign: "middle" }}>
                          {/* add color only buyOffer that are yet to Expire */}
                          {data.buyerOfferExpiresDate?.seconds * 1000 >=
                            Date.now() && !data.dateOfferAccepted ? (
                            <span style={{ color: "#00cdc9" }}>
                              {" "}
                              {parseFloat(
                                data.buyerTreeOfferAmount
                              ).toLocaleString()}{" "}
                            </span>
                          ) : (
                            <span>
                              {" "}
                              {parseFloat(
                                data.buyerTreeOfferAmount
                              ).toLocaleString()}{" "}
                            </span>
                          )}
                        </td>
                        <td style={{ verticalAlign: "middle" }}>
                          <div>
                            <img src="/dudeCoin.png" alt="" />
                            <div className="tiny-font">
                              DUDE <br />
                              Dude Token
                            </div>
                          </div>
                        </td>
                        <td style={{ verticalAlign: "middle" }}>
                          <div>{data.buyerPercentageBuyIn} %</div>
                        </td>
                        <td style={{ verticalAlign: "middle" }}>
                          {/* buyOffer State/Status */}
                          {data.buyerOfferExpiresDate?.seconds * 1000 <=
                            Date.now() && !data.dateOfferAccepted ? (
                            <div>
                              <div className="text-center noWrap">
                                <span
                                  style={{
                                    color: "#00cdc9",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {" "}
                                  [{" "}
                                </span>{" "}
                                Expired{" "}
                                <span
                                  style={{
                                    color: "#00cdc9",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {" "}
                                  ]{" "}
                                </span>
                              </div>
                            </div>
                          ) : data.dateOfferAccepted ? (
                            <div>
                              <div className="text-center noWrap">
                                <span
                                  style={{
                                    color: "#00cdc9",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {" "}
                                  [{" "}
                                </span>{" "}
                                Accepted{" "}
                                <span
                                  style={{
                                    color: "#00cdc9",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {" "}
                                  ]{" "}
                                </span>
                              </div>
                            </div>
                          ) : (
                            <div className="position-relative">
                              <div className="w-100 text-center">
                                <button
                                  onClick={() =>
                                    handleAcceptBuyOffer(data.offerId)
                                  }
                                  className="button mx-auto noWrap  d-inline-block w-75  button-orangeGradient"
                                >
                                  Accept
                                </button>
                                <br />
                                <button
                                  onClick={() => {
                                    setShowPop(!showPop);
                                    setBuyInOfferData({
                                      buyInOfferId: data.offerId,
                                      buyerOfferExpiresDate:
                                        data.buyerOfferExpiresDate,
                                      buyerUserName: data.buyerBranchUserName,
                                    });
                                  }}
                                  className="button mx-auto noWrap d-inline-block w-75 mt-3 black-button"
                                >
                                  Make counter roposals
                                </button>
                              </div>
                            </div>
                          )}
                        </td>
                      </tr>
                    ))
                ) : (
                  <tr className="position-relative overflow-hidden">
                    <td rowSpan="10" style={{ verticalAlign: "middle" }}>
                      <div>No Offer</div>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </Layout>

      {showPop && (
        <Popup
          buyInOfferData={buyInOfferData}
          userId={currentUser && currentUser.uid}
          userProfile={userProfile}
          onClose={closeProposalModal}
        />
      )}
    </>
  );
}

import { onSnapshot } from "@firebase/firestore";
import React, { useEffect } from "react";
import { NavLink, Navigate } from "react-router-dom";
import useAuth from "../context/authContext";
import { userBalanceObject } from "../services/data/dbCollections";
import { userBalanceQdoc } from "../services/firebase/collection";
import Header from "./header";

export default function Index({
  children,
  current,
  showSubHeader,
  title,
  hideRightNav = false,
}) {
  
  const { currentUser, setCurrentUser, isLoadingUser, setUserBalance } =
    useAuth();

  // get userBalance
  useEffect(() => {
    let unsubscribe = () => {};
    if (currentUser) {
      unsubscribe = onSnapshot(userBalanceQdoc(currentUser.uid), (doc) => {
        setUserBalance(doc.data() || userBalanceObject);
      });
    }
    return () => {
      unsubscribe();
    };
  }, [currentUser, setUserBalance]);

  if (isLoadingUser) {
    return (
      <div style={{ width: "50px", margin: "auto", paddingTop: "20%" }}>
        <div style={{ width: "150px" }}> Loading ... </div>
      </div>
    );
  } else {
    if (!isLoadingUser && !currentUser) {
      return <Navigate to="/login" />;
    }

    return (
      <div className="col-lg-10 mb-5 mx-auto ">
        <Header hideRightNav={hideRightNav} />
        <div className="clearfix"></div>

        <div className="mt-3" style={{ fontWeight: "bold" }}>
          {title}
        </div>
        {showSubHeader && (
          <>
            <div className="w-100 text-right mb-4">
              <u>
                Your Buy-In Offers
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  style={{ width: "40px" }}
                  width={61}
                  height={46}
                  viewBox="0 0 61 46"
                >
                  <g
                    id="Polygon_4"
                    data-name="Polygon 4"
                    transform="translate(61 46) rotate(180)"
                    fill="#fff"
                  >
                    <path
                      d="M 53.5484504699707 44.50000381469727 L 7.451551914215088 44.50000381469727 C 6.515172004699707 44.50000381469727 5.691731929779053 44.00745010375977 5.248851776123047 43.18242263793945 C 4.805972099304199 42.35738372802734 4.850502014160156 41.39890289306641 5.3679518699646 40.61847305297852 L 28.41639137268066 5.856882095336914 C 28.88769149780273 5.146071910858154 29.64714241027832 4.738401889801025 30.50000190734863 4.738401889801025 C 31.35286140441895 4.738401889801025 32.11231231689453 5.146071910858154 32.58361053466797 5.856882095336914 L 55.63206100463867 40.61847305297852 C 56.14951324462891 41.39890289306641 56.19403076171875 42.35738372802734 55.75115203857422 43.18242263793945 C 55.30827331542969 44.00745010375977 54.48483276367188 44.50000381469727 53.5484504699707 44.50000381469727 Z"
                      stroke="none"
                    />
                    <path
                      d="M 30.50000190734863 6.2384033203125 C 30.29952239990234 6.2384033203125 29.92466163635254 6.296520233154297 29.66655158996582 6.685791015625 L 6.618110656738281 41.44738006591797 C 6.330501556396484 41.88115310668945 6.464813232421875 42.27613067626953 6.570472717285156 42.47296142578125 C 6.676132202148438 42.66979217529297 6.931102752685547 43 7.451553344726562 43 L 53.5484504699707 43 C 54.06890106201172 43 54.32387161254883 42.6697998046875 54.42953109741211 42.47296142578125 C 54.53519058227539 42.27613067626953 54.66950225830078 41.88116073608398 54.38189315795898 41.44738006591797 L 31.33343124389648 6.685791015625 C 31.07533264160156 6.296520233154297 30.70048141479492 6.2384033203125 30.50000190734863 6.2384033203125 M 30.5 3.2384033203125 C 31.7713623046875 3.2384033203125 33.042724609375 3.834926605224609 33.83376312255859 5.027973175048828 L 56.88220977783203 39.7895622253418 C 58.64516067504883 42.44843292236328 56.73868179321289 46 53.5484504699707 46 L 7.451553344726562 46 C 4.261322021484375 46 2.354843139648438 42.44843292236328 4.117782592773438 39.7895622253418 L 27.16623306274414 5.027973175048828 C 27.957275390625 3.834926605224609 29.22863960266113 3.2384033203125 30.5 3.2384033203125 Z"
                      stroke="none"
                      fill="#707070"
                    />
                  </g>
                </svg>
              </u>
            </div>

            <div className="col header-dashboard p-2 mb-5">
              <div className="row px-3 py-2">
                <div className="col-xl-5 mb-xl-0 mb-4 d-flex alignycenter">
                  <div
                    style={{
                      backgroundImage: `url(https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)`,
                    }}
                    className="profile-image"
                  ></div>
                  <div className="font40 ml-3">Meta Item NFT Id: 2342</div>
                </div>

                <div className="col-md col-6 mt-md-0 mt-3">
                  <NavLink to="/trees">
                    <div
                      className="circle font-weight-bold"
                      style={{
                        color: "#00CDC9",
                        borderColor: current == 0 ? "#00CDC9" : "",
                        borderWidth: "4px",
                      }}
                    >
                      40.1k
                    </div>
                    <div
                      className="font15 text-center mt-2"
                      style={{ color: current == 0 ? "#00CDC9" : "" }}
                    >
                      Royalty Trees Earned Cash Out
                    </div>
                  </NavLink>
                </div>
                <div className="col-md col-6 mt-md-0 mt-3">
                  <NavLink to="/proposal">
                    <div className="circle">40.1k</div>
                    <div className="font15 text-center mt-2">
                      View Decision Proposals
                    </div>
                  </NavLink>
                </div>
                <div className="col-md col-6 mt-md-0 mt-3">
                  <NavLink to="/offers">
                    <div
                      className="circle"
                      style={{
                        color: "#00CDC9",
                        borderColor: current == 2 ? "#00CDC9" : "",
                        borderWidth: "4px",
                      }}
                    >
                      40.1k
                    </div>
                    <div
                      className="font15 text-center mt-2"
                      style={{ color: current == 2 ? "#00CDC9" : "" }}
                    >
                      View Buy-in Offers
                    </div>
                  </NavLink>
                </div>
                <div className="col-md col-6 mt-md-0 mt-3">
                  <NavLink to="/owned">
                    <div className="circle ">40.1k</div>
                    <div className="font15 text-center mt-2">
                      Change % Owned By You
                    </div>
                  </NavLink>
                </div>
              </div>
            </div>
          </>
        )}
        <div className="clearfix"></div>
        <div>{children}</div>
      </div>
    );
  }
}

import React, { useEffect } from "react";
import useAuth from "../context/authContext";
import { toast } from "react-toastify";
import { NavLink } from "react-router-dom";
import { clearUserData } from "../services/storage";
import { useMetaMaskHook } from "../services/hooks/useMetaMaskHook";
import { useAccount } from "../services/hooks/useAccount";
import useMetaMask from "../context/metaMaskContext";

export default function Header({ hideRightNav }) {

  const { signOut } = useAuth();
  const { setWalletAccountInfo, walletAccountInfo } = useMetaMask()
  // listen for events
  const { connected } = useMetaMaskHook()
  const { web3Account } = useAccount(connected);

  const logout = async () => {
    try {
      await signOut()
      clearUserData()
      window.location.reload();
    } catch (err) {
      toast.error("Unable to logout, try again.")
    }
  };

  useEffect(()=> {
    if(web3Account){
      setWalletAccountInfo({...walletAccountInfo, ...web3Account})
    }
  }, [web3Account])

  return (
    <header className="w-100 my-4 top-header">
      <div className="float-left">
        <NavLink to="/">
          <img src="/logo1.png" className="img-header" alt="" />
        </NavLink>
      </div>
      {
        !hideRightNav &&
        <div className="float-right">
          <div className="" id="menu">
            <a className="d-inline-block ml-4 align-middle" href="#">
              Support
            </a>
            <a className="d-inline-block ml-4 align-middle" href="#">
              FAQs
            </a>
            <a onClick={logout} class="d-inline-block ml-4 align-middle" href="#">
              Log out
            </a>
          </div>
        </div>
      }
    </header>
  );

}


export const buyInOfferObject = {
    blockchainNetwork: "",
    blockchainNetworkId: "",
    buyerBranchUserName: "",
    buyerOfferExpiresDate: "",
    buyerPercentageBuyIn: "",
    buyerProfilePic: "",
    buyerTreeOfferAmount: "",
    buyerWalletAddress: "",
    completedTransactionId: "",
    dateOfferAccepted: "",
    itemId: "",
    itemOwnerBranchUsername: "",
    itemPicURL: "",
    itemTitle: "",
    offerId: "VgS59yiAXI1PDl1BsTl3",
    sellerBranchUsername: "",
    sellerProfilePic: "",
    sellerWalletAddress: "",
}

export const userWalletAccountAddress = {
    blockchainAccountId: "",
    blockchainNetwork: "",
    blockchainNetworkId: "",
    branchUserName: "",
    dateUserAgreedTolinkWalletAddressId: "",
    dateUserDeletedLink: "",
    userId: "",
    userProfilePic: "",
    userWalletAddress: "",
}

export const userBalanceObject = {
    SeedDiamonds: 0,
    Trees: 0,
    WithdrawableTrees: 0
}

export const buyInCounterOfferObject = {
    buyerBranchUserNameLower: "",
    counterOfferAmount: "",
    counterOfferPercentage: "",
    counterOfferTokenSymbol: "Diamond",
    creatorBackerBranchusernameLower: "",
    creatorBackerProfilePic: "",
    creatorBackerUserId: "",
    dateTimeCounterOfferCreate: "",
    dateTimeCounterOfferExpires: "",
    isCreator: true,
    itemId: "",
    offerId: ""
}

export const userProfileObject = {
    BranchUsername: "hzariki",
    BranchUsernameLower: "hzariki",
    Email: "hzariki@gmail.com",
    FirstName: "YUHAO",
    IsSysAdmin: false,
    LastLoginCountryCode: "US",
    LastName: "ZOU",
    Name: "YUHAO ZOU",
    ParentBranchUsername: "first1shappy",
    ParentBranchUsernameLower: "first1shappy",
    ParentEmail: "REMOVEDdonotuse888888@lt.com",
    ParentFirstName: "Ashley",
    ParentIsSysAdmin: true,
    ParentLastName: "Turing",
    ParentName: "Ashley Turing",
    PictureUrl: "/users/profilepics/-!.png",
    deviceToken: "e-XBMKXuQlabbQb1lrW3je:APA91bFqlTXLvWrmAYMWUjYOgLBrax2qXEBjK659wZVkrNsOrL2jN01cl5aoTDQsCrPyu6UZlMLyAb_FtwUGq8OsUHv5gq_FgRHyVV3ZJ4NepIIQiNifWhS_GCoC-vIRksKVNkFGIP52",
    isInfluencer: false,
    userId: "0qrK78kboogcpAX3Wj9XutqUd8g2",
}

export const itemObject = {
    itemId: "1",
    mediaURL: "https://livetree.azureedge.net/app/",
    itemTitle: "live_tree_item"
}

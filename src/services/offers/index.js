import { collection, doc, onSnapshot, setDoc, Timestamp, updateDoc } from "@firebase/firestore";
import { db } from "../firebase"
import { buyInOfferObject } from "../data/dbCollections";

const table = "itemBuyInOffer";

export const addNewBuyInOffer = async (dataToAdd) => {
    try {
        const docQuery = doc(collection(db, table));
        const offerId = docQuery.id;
        await setDoc(docQuery, {
            ...buyInOfferObject,
            offerId,
            ...dataToAdd,
            buyerOfferExpiresDate: Timestamp.fromDate(new Date(dataToAdd.buyerOfferExpiresDate))
        });
        console.log('successfully added new Offer')
    } catch (err) {
        console.log(err)
    }

}

export const updateBuyInOffer = async (offerId, dataToUpdate) => {
    try {

        if (dataToUpdate.dateOfferAccepted) {
            dataToUpdate.dateOfferAccepted = Timestamp.fromDate(new Date(dataToUpdate.dateOfferAccepted))
        }
        const docRef = doc(db, table, offerId);
        await updateDoc(docRef, {
            ...dataToUpdate
        });
        console.log('successfully updated Offer')
    } catch (err) {
        console.log(err)
    }

}

export const buyInOfferCollection = () => {
    return collection(db, table);
}



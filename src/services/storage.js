
const saveUser = user => sessionStorage.setItem('liveTree_user', JSON.stringify(user));

const saveIdToken = idToken => sessionStorage.setItem('liveTree_idToken', idToken);

const getUser = () => JSON.parse(sessionStorage.getItem('liveTree_user'));

const getIdToken = () => sessionStorage.getItem('liveTree_idToken');

const saveAccount = (account, blockchainNetwork, blockchainNetworkId) => sessionStorage.setItem('liveTree_account', JSON.stringify({account, blockchainNetwork, blockchainNetworkId}))

const getAccount = () => JSON.parse(sessionStorage.getItem('liveTree_account')) || {account: null, blockchainNetwork: null, blockchainNetworkId: null};

const clearUserData = () => {
    sessionStorage.removeItem('liveTree_account');
};

export { saveUser, getUser, saveAccount, getAccount, clearUserData, saveIdToken, getIdToken };
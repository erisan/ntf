
import { collection, setDoc, doc, query, where, getDocs, updateDoc, Timestamp } from "firebase/firestore";
import { toast } from "react-toastify";
import { db } from "../firebase";
import { getAccount, saveAccount } from "../storage";

export const allChainNetworks = {
    '1': 'Ethereum Mainnet',
    '3': 'Ropsten Test Network',
    '4': 'Rinkeby Test Network',
    '5': 'Goerli Test Network',
    '42': 'Kovan Test Network',
    '42220': 'CELO',
    '1285': 'Moonriver',
    '1284': 'Moonbeam',
}

export const allWallets = []

export const supportWallets = {
    "1": {
        chain: 'Ethereum Mainnet',
        label: 'ETH',
        value: 4000,
        chainId: "1"
    },
    '3': {
        chain: 'Ropsten Test Network',
        label: 'ROP',
        value: 4000,
        chainId: "3"
    },
    '4': {
        chain: 'Rinkeby Test Network',
        label: 'RIN',
        value: 4000,
        chainId: "4"
    },
    '5': {
        chain: 'Goerli Test Network',
        label: 'GOE',
        value: 4000,
        chainId: "5"
    },
    '42': {
        chain: 'Kovan Test Network',
        label: 'KOV',
        value: 4000,
        chainId: "42"
    },
    "42220": {
        chain: 'CELO',
        label: 'CELO',
        value: 4.95,
        chainId: "42220"
    },
    "1285": {
        chain: 'Moonriver',
        label: 'MOVR',
        value: 300,
        chainId: "1285"
    },
    "1284": {
        chain: 'Moonbeam',
        label: 'GLMR',
        value: 400,
        chainId: "1284"
    }
}

export const handleChainChange = ({ userId, setWalletAccountInfo }) => (chainId) => {

    const { account } = getAccount();
    let address;

    if (Object.keys(supportWallets).includes(parseInt(chainId).toString())) {

        if (account) {

            address = account;
            let chainNetwork = allChainNetworks[parseInt(chainId)];
            if (userId && address) {
                saveAccount(address, chainNetwork, parseInt(chainId));
                setWalletAccountInfo({ account: address, blockchainNetwork: chainNetwork, blockchainNetworkId: parseInt(chainId) })
                updateWalletAccount(db, userId, address, {
                    blockchainNetwork: chainNetwork,
                    blockchainNetworkId: parseInt(chainId),
                })
                    .then((status) => {
                        const wallet = supportWallets[parseInt(chainId).toString()];
                        toast.success(`${wallet.chain}`)
                        //  reload page
                        // if(status) window.location.reload();
                    })
                    .catch(console.log)
            }

        }
    } else {
        toast.error("not supported")
    }

}

export const handleAccountsChange = ({ userId, ethereum, setWalletAddress, setWalletAccountInfo }) => async (accounts) => {
    // Time to reload your interface with accounts[0]!
    let chainId = 1;
    if (ethereum.chainId) {
        chainId = parseInt(ethereum.chainId);
    }
    let chainNetwork = allChainNetworks[chainId];
    let address;
    if (accounts.length) {
        address = accounts[0];
        saveAccount(address, chainNetwork, chainId);
        setWalletAccountInfo({ account: address, blockchainNetwork: chainNetwork, blockchainNetworkId: chainId })
        setWalletAddress(address)
        if (userId && address) {
            await setWalletAccount(db, userId, address, {
                blockchainAccountId: "",
                blockchainNetwork: chainNetwork,
                blockchainNetworkId: chainId,
                branchUserName: "",
                userProfilePic: "",
            })
        }
    }

}

export async function connectMetaMask() {
    try {
        const { ethereum } = window
        if (ethereum && ethereum.isMetaMask) {
            await ethereum.request({ method: 'eth_requestAccounts' });
        } else {
            toast.info("MetaMask has not been installed")
        }
    } catch (ex) {
        if (ex.code == -32002) {
            toast.info('application awaiting MetaMask wallet permission');
        } else if (ex.code === 4001) {
            // EIP-1193 userRejectedRequest error
            toast.info('Please connect to MetaMask.');
        }
    }
}


export async function disconnectMetaMask(deactivate) {
    try {
        deactivate()
    } catch (ex) {
        console.log(ex)
    }
}

export async function getActiveAccount() {

}

export async function setWalletAccount(db, userId, address, dataToAdd) {

    try {
        const table = "userWalletAccountAddress";

        const q = query(collection(db, table), where('userWalletAddress', '==', address));
        const querySnapshot = await getDocs(q);
        const walletAccount = []
        querySnapshot.forEach((doc) => {
            walletAccount.push(doc.data())
        });

        console.log("switching account.................")

        if (walletAccount.length > 0) { // update collection
            // await updateDoc(q, dataToAdd);
            console.log('successfully updated wallet account')
        } else { // Add a new document with a generated id. 
            await addWalletAccount(db, userId, address, dataToAdd)
        }
    } catch (err) {
        console.log(err)
    }
}

export async function updateWalletAccount(db, userId, address, dataToUpdate) {

    try {
        const table = "userWalletAccountAddress";

        const q = query(collection(db, table), where('userWalletAddress', '==', address));
        const querySnapshot = await getDocs(q);

        querySnapshot.forEach(async (docResp) => {
            if (docResp.id) {
                const updateRef = doc(db, table, docResp.id);
                await updateDoc(updateRef, dataToUpdate);
            }
        });
        console.log('successfully updated wallet account')
        return true;
    } catch (err) {
        console.log(err)
    }
}


export async function addWalletAccount(db, userId, address, dataToAdd) {
    try {
        const table = "userWalletAccountAddress";
        dataToAdd.userId = userId;
        dataToAdd.userWalletAddress = address;
        dataToAdd.dateUserAgreedTolinkWalletAddressId = Timestamp.fromDate(new Date());
        dataToAdd.dateUserDeletedLink = ''

        const docQuery = doc(collection(db, table));
        await setDoc(docQuery, dataToAdd);
        console.log('successfully added new wallet account')
    } catch (err) {
        console.log(err)
    }
}


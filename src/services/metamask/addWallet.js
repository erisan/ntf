

export const CELO_PARAMS = {
    chainId: '0xa4ec',
    chainName: 'Celo',
    nativeCurrency: { name: 'Celo', symbol: 'CELO', decimals: 18 },
    rpcUrls: ['https://forno.celo.org'],
    blockExplorerUrls: ['https://explorer.celo.org/'],
    iconUrls: ['future'],
};

export const MOVR_PARAMS = {
    chainId: '0x505',
    chainName: 'Moonriver',
    nativeCurrency: { name: 'Moonriver', symbol: 'MOVR', decimals: 18 },
    rpcUrls: ['https://rpc.moonriver.moonbeam.network'],
    blockExplorerUrls: ['https://moonriver.moonscan.io'],
    iconUrls: ['future'],
};

export const addWalletToMetaMask = async (ethereum, network_params) => {
    await ethereum.request({
        method: 'wallet_addEthereumChain',
        params: [network_params],
    });
}


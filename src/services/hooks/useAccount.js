import React, { useCallback, useEffect } from "react";
import { allChainNetworks } from "../metamask/actions";

export const useAccount = (connected, refreshInterval = 5000) => {

    const [web3Account, setWeb3Account] = React.useState(null);

    const getAddress = async () => {
        const accounts = await window.web3.eth.getAccounts();
        return accounts[0];
    };
    const getAccounBalance = async (address) => {
        return await window.web3.eth.getBalance(address);
    };
    const getChainId = async () => {
        return await window.web3.eth.getChainId();
    };

    const checkWeb3Account = useCallback(async () => {
        if (!connected) {
            setWeb3Account(null);
            return;
        }
        try {
            console.log("getting address balance, and chainId.............")
            const address = await getAddress();
            const balance = await getAccounBalance(address);
            const chanId = await getChainId();
            setWeb3Account({ address, balance, blockchainNetwork: allChainNetworks[parseInt(chanId)], blockchainNetworkId: parseInt(chanId) });
        } catch (err) {
            console.log(err)
        }
    }, [connected]);

    useEffect(() => {
        checkWeb3Account();
    }, [connected, checkWeb3Account]);

    return { web3Account, checkWeb3Account };

};


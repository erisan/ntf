import { React, useCallback, useEffect, useState } from "react";
import Web3 from "web3";
import useAuth from "../../context/authContext";
import useMetaMask from "../../context/metaMaskContext";
import { handleAccountsChange, handleChainChange } from "../metamask/actions";


export function useMetaMaskHook() {

  const [connected, setConnected] = useState(null);

  const { currentUser } = useAuth();
  const { setWalletAddress, setWalletAccountInfo } = useMetaMask()

  const userId = currentUser?.uid;
  const { ethereum } = window;

  // connect to metamask
  const connectProvider = useCallback(async () => {

    const handleError = () => {
      setConnected(false);
    };

    try {
      window.web3 = new Web3(window.ethereum || window.web3.currentProvider);
      // await window.ethereum.enable();
      console.log('provider connected ............')
      setConnected(true);
    } catch (e) {
      handleError();
    }

  }, []);

  useEffect(() => {
    if (ethereum) {
      // connectProvider
      connectProvider();
      // listen for accounts changes event
      ethereum.on('accountsChanged', handleAccountsChange({ userId, ethereum, setWalletAddress, setWalletAccountInfo }));
      // listen for chain changed event
      ethereum.on('chainChanged', handleChainChange({ userId, setWalletAccountInfo }));
    }
    return () => {
      // clean up
      window.removeEventListener('accountsChanged', handleAccountsChange({ userId, ethereum, setWalletAddress, setWalletAccountInfo }));

      window.removeEventListener('chainChanged', handleChainChange({ userId, setWalletAccountInfo }))
    }
  }, [ethereum])


  return { connected };
}

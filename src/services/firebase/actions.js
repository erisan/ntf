import { collection, doc, setDoc, Timestamp } from "@firebase/firestore"
import { db } from "."
import { buyInCounterOfferObject, userBalanceObject, userProfileObject } from "../data/dbCollections"
import { buyInCounterOfferTable, userBalanceTable, userProfileDoc, userProfileTable } from "./collection"


export const updateUserBalance = async (userId, dataToUpdate = {}) => {
    const queryDoc = doc(db, userBalanceTable, userId)
    await setDoc(queryDoc, {
        ...userBalanceObject,
        ...dataToUpdate
    })
}

export const addNewBuyInCounterOffer = async (dataToAdd = {}) => {
    try {
        const docQuery = doc(collection(db, buyInCounterOfferTable));
        const offerId = docQuery.id;
        await setDoc(docQuery, {
            ...buyInCounterOfferObject,
            ...dataToAdd,
            dateTimeCounterOfferCreate: Timestamp.fromDate(new Date())
        });
        console.log('successfully added Counter proposal ')
    } catch (err) {
        console.log(err)
    }
}


export const addUserProfile = async (userId, dataToAdd = {}) => {
    try {
        const docQuery = doc(db, userProfileTable, userId);
        await setDoc(docQuery, {
            ...userProfileObject,
            ...dataToAdd,
            userId,
            BranchUsername: "sam",
            BranchUsernameLower: "sam",
            Email: "samvwede@gmail.com",
            FirstName: "vwede",
        });
        console.log('successfully added userProfile ')
    } catch (err) {
        console.log(err)
    }
}

// export const addItem = async (userId, dataToAdd = {}) => {
//     try {
//         const docQuery = doc(db, userProfileTable, userId);
//         await setDoc(docQuery, {
//             ...userProfileObject,
//             ...dataToAdd,
//             userId,
//             BranchUsername: "sam",
//             BranchUsernameLower: "sam",
//             Email: "samvwede@gmail.com",
//             FirstName: "vwede",
//         });
//         console.log('successfully added userProfile ')
//     } catch (err) {
//         console.log(err)
//     }
// }


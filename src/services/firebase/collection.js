import { doc } from "@firebase/firestore";
import { db } from ".";

export const userBalanceTable = 'userBalance';
export const buyInCounterOfferTable = 'itemBuyInOfferCounterOffer';
export const userProfileTable = 'userProfile';
export const itemTable = 'item';

export const userBalanceQdoc = (userId) => doc(db, userBalanceTable, userId)

export const itemBuyInOfferCounterOfferQdoc = (userId) => doc(db, buyInCounterOfferTable, userId)

export const userProfileDoc = (userId) => doc(db, userProfileTable, userId)

// export const itemDoc = 
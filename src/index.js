import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import "./global.scss"
import App from './App';
import reportWebVitals from './reportWebVitals';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { AuthProvider } from './context/authContext';
import { OtherProvider } from "./context/otherContext"
import { MetaMaskProvider } from './context/metaMaskContext';


ReactDOM.render(
  <React.StrictMode>
      <AuthProvider>
        <MetaMaskProvider>
          <OtherProvider>
            <ToastContainer />
            <App />
          </OtherProvider>
        </MetaMaskProvider>
      </AuthProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

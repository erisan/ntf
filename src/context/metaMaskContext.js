
import React, { createContext, useContext, useEffect, useMemo, useState } from "react";
import { getAccount } from "../services/storage";


const MetaMaskContext = createContext();

export function MetaMaskProvider({ children }) {

  const [walletAddress, setWalletAddress] = useState();
  const [walletAccountInfo, setWalletAccountInfo] = useState({});

  const contextAction = useMemo(() => ({

  }), [])

  useEffect(() => {
    const data = getAccount();
    setWalletAddress(data.account);
    setWalletAccountInfo(data);
  }, [])

  return (
    <MetaMaskContext.Provider
      value={{
        ...contextAction,
        walletAddress,
        setWalletAddress,
        walletAccountInfo,
        setWalletAccountInfo
      }}
    >
      {children}
    </MetaMaskContext.Provider>
  );
}

export default function useMetaMask() {
  return useContext(MetaMaskContext);
}

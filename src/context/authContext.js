import React, { createContext, useContext, useEffect, useState } from "react";
import {
  createUserWithEmailAndPassword,
  signOut,
  signInWithEmailAndPassword,
  updateEmail,
  onAuthStateChanged,
} from "firebase/auth";
import { auth } from "../services/firebase";
import { userBalanceObject } from "../services/data/dbCollections";
import { userProfileDoc } from "../services/firebase/collection";
import { getDoc } from "firebase/firestore";

const AuthContext = createContext();

export function AuthProvider({ children }) {

  const [currentUser, setCurrentUser] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isLoadingUser, setIsLoadingUser] = useState(true)
  const [userBalance, setUserBalance] = useState(userBalanceObject)
  const [userProfile, setUserProfile] = useState({});

  useEffect(() => {
    onAuthStateChanged(auth, async (user) => {
      if (user) {
        setCurrentUser(user);
        // get user profile
        const u_profile = await getDoc(userProfileDoc(user.uid));
        if (u_profile.exists()) {
          setUserProfile(u_profile.data());
        }
      }
      setIsLoadingUser(false)
    });
    return () => {
      setCurrentUser({})
    }
  }, [])

  const functions = {
    createUserWithEmailAndPassword: (email, password) => {
      return createUserWithEmailAndPassword(auth, email, password);
    },
    signInWithEmailAndPassword: (email, password) => {
      return signInWithEmailAndPassword(auth, email, password);
    },
    updateEmail: (email) => updateEmail(auth.currentUser, email),
    signOut: () => signOut(auth),
  };

  return (
    <AuthContext.Provider
      value={{
        auth,
        currentUser,
        userProfile,
        setCurrentUser,
        ...functions,
        isAuthenticated,
        setIsAuthenticated,
        isLoadingUser,
        setUserBalance,
        userBalance
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export default function useAuth() {
  return useContext(AuthContext);
}

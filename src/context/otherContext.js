import React, { createContext, useContext, useEffect } from "react";
import { getFirestore, addDoc, collection, getDocs } from "firebase/firestore";

const db = getFirestore();

const OtherContext = createContext();

export function OtherProvider({ children }) {
  const functions = {
    createNewBlog: () => {
      return addDoc(collection(db, "isData"), {
        name: "Sam",
        class: "332",
        age: 12,
      });
    },
    getBlog: () => {
      return getDocs(collection(db, "isData"));
    },
  };

  // useEffect(() => {
  //   functions.getBlog().then((snapShot) => {
  //     console.log(
  //       "Success",
  //       snapShot.docs.forEach((data) => {
  //         console.log(data.data());
  //       })
  //     );
  //   });
  //   // const querySnapshot = await getDocs(collection(db, "users"))
  // }, []);

  return (
    <OtherContext.Provider value={{ ...functions }}>
      {children}
    </OtherContext.Provider>
  );
}

export default function useAuth() {
  return useContext(OtherContext);
}

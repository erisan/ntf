const { ethers, upgrades } = require("hardhat");

const { expect } = require("chai");

describe("RoyalItemManager BuyIn", async function (){

    beforeEach(async function(){
        const Setting = await ethers.getContractFactory("Settings");
        const [owner, addr0] = await ethers.getSigners();
        this.setting = await Setting.deploy();
        await this.setting.deployed();
    
        const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
        const royaltyItemManager = await RoyaltyItemManager.deploy(this.setting.address);
        await royaltyItemManager.deployed();
    
    
        const RoyaltyERC721 = await ethers.getContractFactory("RoyaltyERC721");
        const royaltyNFT = await RoyaltyERC721.deploy();
        await royaltyNFT.deployed();
    
    
        //ESCROW-ADMIN: 0x10E813435B4b54D789E635CC04073d32eF83539C
        const EscrowManager = await ethers.getContractFactory("EscrowManager");
        this.escrowMgr = await upgrades.deployProxy(EscrowManager, [addr0.address]);
        await this.escrowMgr.deployed();
         
        const RoyaltyItemProxyFactory = await ethers.getContractFactory("RoyalItemFactoryProxy");
        this.royaltyItemProxyFactory = await RoyaltyItemProxyFactory.deploy(this.setting.address, royaltyItemManager.address, royaltyNFT.address, this.setting.address, this.escrowMgr.address);
        await this.royaltyItemProxyFactory.deployed();
      });

    it("should register buyIn offers", async function(){

        const trx = await this.royaltyItemProxyFactory.mint(
            [
              10,
              ethers.utils.parseUnits(10e8.toPrecision(), "18"),
              ethers.utils.parseUnits((0.008).toPrecision(), "18"),
              ethers.utils.parseUnits(0.012.toPrecision(), "18"),
              "SafaRI",
              "SFRI",
              "logoURL",
              "https://jpg.media.com/?id=20",
              "https://some.media.com/?id=20&format=json",
              "https://jpg.media.com/?id=20",
              "https://jpg.media.com/?id=20",
              "https://app.media.com/view?=20",
              ethers.utils.parseUnits(5e1.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              "phil",
              10,
              "offer-123",
            ]
        );

        await trx.wait();

        const [owner, buyer] = await ethers.getSigners();

        const itemMgrIndex = await this.royaltyItemProxyFactory.userTokenExists(owner.address, "SafaRI", "SFRI");
    
        const mgrAddr = await this.royaltyItemProxyFactory.royaltyItemManager(itemMgrIndex[1]);
    
        const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
    
        const itemMgr = await RoyaltyItemManager.attach(mgrAddr);

        let buyInTrx = await itemMgr.connect(buyer).MakeBuyInOffer(
            10, 
            "offer-123", 
            ethers.utils.parseUnits(1e7.toPrecision(),"18"), 
            ethers.utils.parseEther("0.1"), 
            Math.floor(Date.now()/1000)+100,
            "0x0000000000000000000000000000000000000000",
            {value: ethers.utils.parseEther("0.1")}
        );
        
        await buyInTrx.wait();

        const offer = await itemMgr.buyInOffer("offer-123");

        // buyInOffers[offerId].offerId,
        // buyInOffers[offerId].itemId,
        // buyInOffers[offerId].buyer,
        // buyInOffers[offerId].amountERC20,
        // buyInOffers[offerId].tokenAmount,
        // buyInOffers[offerId].percentage

        expect(offer[0]).to.equal("offer-123");
        expect(offer[1]).to.equal(10);
        expect(offer[2]).to.equal(buyer.address);
        expect(offer[3]).to.equal(ethers.utils.parseEther("0.1"));
        expect(offer[4]).to.equal(ethers.utils.parseUnits(1e7.toPrecision(),"18"));
        expect(await itemMgr.balanceOf(buyer.address)).to.equal(0);
    });


    it("should accept existing buyIn offers", async function(){

        const trx = await this.royaltyItemProxyFactory.mint(
            [
              10,
              ethers.utils.parseUnits(10e8.toPrecision(), "18"),
              ethers.utils.parseUnits((0.008).toPrecision(), "18"),
              ethers.utils.parseUnits(0.012.toPrecision(), "18"),
              "SafaRI",
              "SFRI",
              "logoURL",
              "https://jpg.media.com/?id=20",
              "https://some.media.com/?id=20&format=json",
              "https://jpg.media.com/?id=20",
              "https://jpg.media.com/?id=20",
              "https://app.media.com/view?=20",
              ethers.utils.parseUnits(5e1.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              "phil",
              10,
              "offer-123",
            ]
        );

        await trx.wait();

        const [owner, buyer] = await ethers.getSigners();

        const itemMgrIndex = await this.royaltyItemProxyFactory.userTokenExists(owner.address, "SafaRI", "SFRI");
    
        const mgrAddr = await this.royaltyItemProxyFactory.royaltyItemManager(itemMgrIndex[1]);
    
        const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
    
        const itemMgr = await RoyaltyItemManager.attach(mgrAddr);

        const buyInTrx = await itemMgr.connect(buyer).MakeBuyInOffer(
            10, 
            "offer-123", 
            ethers.utils.parseUnits(1e7.toPrecision(),"18"), 
            ethers.utils.parseEther("0.1"), 
            Math.floor(Date.now()/1000)+100, 
            "0x0000000000000000000000000000000000000000",
            {value: ethers.utils.parseEther("0.1")}
        );
        
        await buyInTrx.wait();

        let acceptTrx = await itemMgr.connect(owner).AcceptBuyInOffer(10,"offer-123");

        await acceptTrx.wait();
        
        expect(await itemMgr.balanceOf(buyer.address)).to.equal(ethers.utils.parseUnits(1e7.toString(),"18"));
        expect(await itemMgr.balanceOf(owner.address)).to.equal(ethers.utils.parseUnits(4e7.toString(),"18"));
    });


    it("should fail on attempt to accept accepted offer", async function(){

        const trx = await this.royaltyItemProxyFactory.mint(
            [
              10,
              ethers.utils.parseUnits(10e8.toPrecision(), "18"),
              ethers.utils.parseUnits((0.008).toPrecision(), "18"),
              ethers.utils.parseUnits(0.012.toPrecision(), "18"),
              "SafaRI",
              "SFRI",
              "logoURL",
              "https://jpg.media.com/?id=20",
              "https://some.media.com/?id=20&format=json",
              "https://jpg.media.com/?id=20",
              "https://jpg.media.com/?id=20",
              "https://app.media.com/view?=20",
              ethers.utils.parseUnits(5e1.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              "phil",
              10,
              "offer-123",
            ]
        );

        await trx.wait();

        const [owner, buyer] = await ethers.getSigners();

        const itemMgrIndex = await this.royaltyItemProxyFactory.userTokenExists(owner.address, "SafaRI", "SFRI");
    
        const mgrAddr = await this.royaltyItemProxyFactory.royaltyItemManager(itemMgrIndex[1]);
    
        const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
    
        const itemMgr = await RoyaltyItemManager.attach(mgrAddr);

        const buyInTrx = await itemMgr.connect(buyer).MakeBuyInOffer(
            10, 
            "offer-123", 
            ethers.utils.parseUnits(1e7.toPrecision(),"18"), 
            ethers.utils.parseEther("0.1"), 
            Math.floor(Date.now()/1000)+100, 
            "0x0000000000000000000000000000000000000000",
            {value: ethers.utils.parseEther("0.1")}
        );
        
        await buyInTrx.wait();

        let acceptTrx = await itemMgr.connect(owner).AcceptBuyInOffer(10,"offer-123");

        await acceptTrx.wait();
        
        try{
            let acceptTrx = await itemMgr.connect(owner).AcceptBuyInOffer(10,"offer-123");
            await acceptTrx.wait();
            return Promise.reject("Test failed: Multiple accept should throw exception")
        }catch(e){
            return Promise.resolve();
        }
    });


    it("should release funds to buyer when offer expired", async function(){

        const trx = await this.royaltyItemProxyFactory.mint(
            [
              10,
              ethers.utils.parseUnits(10e8.toPrecision(), "18"),
              ethers.utils.parseUnits((0.008).toPrecision(), "18"),
              ethers.utils.parseUnits(0.012.toPrecision(), "18"),
              "SafaRI",
              "SFRI",
              "logoURL",
              "https://jpg.media.com/?id=20",
              "https://some.media.com/?id=20&format=json",
              "https://jpg.media.com/?id=20",
              "https://jpg.media.com/?id=20",
              "https://app.media.com/view?=20",
              ethers.utils.parseUnits(5e1.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              ethers.utils.parseUnits(0e0.toPrecision(), "18"),
              "phil",
              10,
              "offer-123",
            ]
        );

        await trx.wait();

        const [owner, buyer] = await ethers.getSigners();

        const itemMgrIndex = await this.royaltyItemProxyFactory.userTokenExists(owner.address, "SafaRI", "SFRI");
    
        const mgrAddr = await this.royaltyItemProxyFactory.royaltyItemManager(itemMgrIndex[1]);
    
        const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
    
        const itemMgr = await RoyaltyItemManager.attach(mgrAddr);

        const buyInTrx = await itemMgr.connect(buyer).MakeBuyInOffer(
            10, 
            "offer-123", 
            ethers.utils.parseUnits(1e7.toPrecision(),"18"), 
            ethers.utils.parseEther("0.1"), 
            Math.floor(Date.now()/1000), 
            "0x0000000000000000000000000000000000000000",
            {value: ethers.utils.parseEther("0.1")}
        );

        await buyInTrx.wait();

        const buyerBalance = await buyer.getBalance();

        // console.log("BUYER BALANCE: ", buyerBalance);

        // console.log("ESCROW: ", this.escrowMgr.address);

        const rtx = await this.escrowMgr.ReleaseOffer(itemMgr.address, "offer-123");

        await rtx.wait();

        const balance = await buyer.getBalance();

        // console.log("NB: ", balance);
        
        expect(balance).to.equal(buyerBalance.add(ethers.utils.parseEther("0.1")));
    });




})
const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("RoyaltyItemProxyFactory", function () {

  beforeEach(async function () {
    const Setting = await ethers.getContractFactory("Settings");
    const [owner, addr0] = await ethers.getSigners();
    this.setting = await Setting.deploy();
    await this.setting.deployed();

    const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
    const royaltyItemManager = await RoyaltyItemManager.deploy(this.setting.address);
    await royaltyItemManager.deployed();


    const RoyaltyERC721 = await ethers.getContractFactory("RoyaltyERC721");
    const royaltyNFT = await RoyaltyERC721.deploy();
    await royaltyNFT.deployed();


    //ESCROW-ADMIN: 0x10E813435B4b54D789E635CC04073d32eF83539C
    const EscrowManager = await ethers.getContractFactory("EscrowManager");
    const escrowMgr = await upgrades.deployProxy(EscrowManager, [addr0.address]);
    await escrowMgr.deployed();

    const RoyaltyItemProxyFactory = await ethers.getContractFactory("RoyalItemFactoryProxy");
    this.royaltyItemProxyFactory = await RoyaltyItemProxyFactory.deploy(this.setting.address, royaltyItemManager.address, royaltyNFT.address, this.setting.address, escrowMgr.address);
    await this.royaltyItemProxyFactory.deployed();
  })

  it("should have correct Settings", async function () {
    expect(await this.royaltyItemProxyFactory.getSettings()).to.equal(this.setting.address);
  });

  it("should create RoyaltyItemManager with configured settings", async function () {

    const trx = await this.royaltyItemProxyFactory.mint(
      [
        20,
        ethers.utils.parseUnits(10e8.toPrecision(), "18"),
        ethers.utils.parseUnits((0.008).toPrecision(), "18"),
        ethers.utils.parseUnits(0.012.toPrecision(), "18"),
        "SafaRI",
        "SFRI",
        "logoURL",
        "https://jpg.media.com/?id=20",
        "https://some.media.com/?id=20&format=json",
        "https://jpg.media.com/?id=20",
        "https://jpg.media.com/?id=20",
        "https://app.media.com/view?=20",
        ethers.utils.parseUnits(5e1.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        "phil",
        20,
        "offer123",
      ]
    );
    if (trx) {
      const settings = await this.royaltyItemProxyFactory.getRoyaltyItemManagerSettings(0);
      // Disabled escrow - owner balance change
      expect(settings[0]).to.equal(50);
      expect(settings[1]).to.equal(50);
      expect(settings[2]).to.equal(0);
      // Disabled escrow - escrow balance change
      expect(settings[3]).to.equal(0);
    }
  });

  it("should mint NFTs", async function () {

    // uint256 itemId;
    // uint256 totalSupply;
    // uint256 listPrice;
    // uint fee;
    // string erc20TokenName;
    // string erc20TokenSymbol;
    // string erc20TokenLogoUrl;
    // string nftMediaURL;
    // string nftMetadataJsonUrl;
    // string nftViewStatsJsonUrl;
    // string nftPictureUrl;
    // string nftAppLinkUrl;
    // uint256 ownerInitialPercentage;
    // uint256 escrowMgrPercentage;
    // uint256 buyerPercentage;
    // string branchBuyerUsername;
    // uint256 buyerItemId;
    // uint256 buyerOffer;
    let trx = await this.royaltyItemProxyFactory.mint(
      [
        20,
        ethers.utils.parseUnits(10e8.toPrecision(), "18"),
        ethers.utils.parseUnits((0.008).toPrecision(), "18"),
        ethers.utils.parseUnits(0.012.toPrecision(), "18"),
        "SafaRI",
        "SFRI",
        "logoURL",
        "https://jpg.media.com/?id=20",
        "https://some.media.com/?id=20&format=json",
        "https://jpg.media.com/?id=20",
        "https://jpg.media.com/?id=20",
        "https://app.media.com/view?=20",
        ethers.utils.parseUnits(50e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        "phil",
        20,
        "offer245",
      ]
    );

    trx.wait();

  });


  it("should fail NFT mint if token symbol,name pair exists", async function () {

    // uint256 itemId;
    // uint256 totalSupply;
    // uint256 listPrice;
    // uint fee;
    // string erc20TokenName;
    // string erc20TokenSymbol;
    // string erc20TokenLogoUrl;
    // string nftMediaURL;
    // string nftMetadataJsonUrl;
    // string nftViewStatsJsonUrl;
    // string nftPictureUrl;
    // string nftAppLinkUrl;
    // uint256 ownerInitialPercentage;
    // uint256 escrowMgrPercentage;
    // uint256 buyerPercentage;
    // string branchBuyerUsername;
    // uint256 buyerItemId;
    // uint256 buyerOffer;
    let trx = await this.royaltyItemProxyFactory.mint(
      [
        20,
        ethers.utils.parseUnits(10e8.toPrecision(), "18"),
        ethers.utils.parseUnits((0.008).toPrecision(), "18"),
        ethers.utils.parseUnits(0.012.toPrecision(), "18"),
        "SafaRI",
        "SFRI",
        "logoURL",
        "https://jpg.media.com/?id=20",
        "https://some.media.com/?id=20&format=json",
        "https://jpg.media.com/?id=20",
        "https://jpg.media.com/?id=20",
        "https://app.media.com/view?=20",
        ethers.utils.parseUnits(50e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        "phil",
        20,
        "offer245",
      ]
    );

    trx.wait();

    try {

      trx = await this.royaltyItemProxyFactory.mint(
        [
          20,
          ethers.utils.parseUnits(10e8.toPrecision(), "18"),
          ethers.utils.parseUnits((0.008).toPrecision(), "18"),
          ethers.utils.parseUnits(0.012.toPrecision(), "18"),
          "SafaRI",
          "SFRI",
          "logoURL",
          "https://jpg.media.com/?id=20",
          "https://some.media.com/?id=20&format=json",
          "https://jpg.media.com/?id=20",
          "https://jpg.media.com/?id=20",
          "https://app.media.com/view?=20",
          ethers.utils.parseUnits(50e0.toPrecision(), "18"),
          ethers.utils.parseUnits(0e0.toPrecision(), "18"),
          ethers.utils.parseUnits(0e0.toPrecision(), "18"),
          "phil",
          20,
          "offer245",
        ]
      );

      trx.wait();
      return Promise.reject("Minting duplicate NFT Manager with same token name, symbol pair should fail")
    } catch (e) {
      return Promise.resolve();
    }


  });


  it("should mint RoyaltyItemManager", async function () {

    const trx = await this.royaltyItemProxyFactory.mint(
      [
        20,
        ethers.utils.parseUnits(10e8.toPrecision(), "18"),
        ethers.utils.parseUnits((0.008).toPrecision(), "18"),
        ethers.utils.parseUnits(0.012.toPrecision(), "18"),
        "SafaRI",
        "SFRI",
        "logoURL",
        "https://jpg.media.com/?id=20",
        "https://some.media.com/?id=20&format=json",
        "https://jpg.media.com/?id=20",
        "https://jpg.media.com/?id=20",
        "https://app.media.com/view?=20",
        ethers.utils.parseUnits(50e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        "phil",
        20,
        "offer245",
      ]
    );

    await trx.wait();

    const [owner] = await ethers.getSigners();

    const itemMgrIndex = await this.royaltyItemProxyFactory.userTokenExists(owner.address, "SafaRI", "SFRI");

    const mgrAddr = await this.royaltyItemProxyFactory.royaltyItemManager(itemMgrIndex[1]);

    const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");

    const itemMgr = await RoyaltyItemManager.attach(mgrAddr);

    const details = await itemMgr.detail();

    expect(details[0]).to.equals("SafaRI");

    expect(details[1]).to.equals("SFRI");

  });


  it("should register buyIn offers", async function () {

    const [owner, buyer] = await ethers.getSigners();

    // uint256 itemId,
    // string calldata offerId,
    // uint256 percentage,
    // uint256 tokenAmount,
    // uint256 amountEthOrWeth,
    // uint256 expiry,
    // address creator,
    // address erc20Token

    let buyInTrx = await this.royaltyItemProxyFactory.connect(buyer).MakeBuyInOffer(
      10,
      "offer-123",
      10,
      ethers.utils.parseUnits(1e7.toPrecision(), "18"),
      ethers.utils.parseEther("0.1"),
      Math.floor(Date.now() / 1000) + 1000,
      owner.address,
      "0x0000000000000000000000000000000000000000",
      { value: ethers.utils.parseEther("0.1") }
    );

    await buyInTrx.wait();

    const offer = await this.royaltyItemProxyFactory.buyInOffer("offer-123");

    // buyInOffers[offerId].offerId,
    // buyInOffers[offerId].itemId,
    // buyInOffers[offerId].buyer,
    // buyInOffers[offerId].amountERC20,
    // buyInOffers[offerId].tokenAmount,
    // buyInOffers[offerId].percentage

    expect(offer[0]).to.equal("offer-123");
    expect(offer[1]).to.equal(10);
    expect(offer[2]).to.equal(buyer.address);
    expect(offer[3]).to.equal(ethers.utils.parseEther("0.1"));
    expect(offer[4]).to.equal(ethers.utils.parseUnits(1e7.toPrecision(), "18"));
    // expect(await itemMgr.balanceOf(buyer.address)).to.equal(0);
  });


  it("should accept existing buyIn offers", async function () {

    const [owner, buyer] = await ethers.getSigners();

    const buyInTrx = await this.royaltyItemProxyFactory.connect(buyer).MakeBuyInOffer(
      10,
      "offer-123",
      10,
      ethers.utils.parseUnits(1e7.toPrecision(), "18"),
      ethers.utils.parseEther("0.1"),
      Math.floor(Date.now() / 1000) + 1000,
      owner.address,
      "0x0000000000000000000000000000000000000000",
      { value: ethers.utils.parseEther("0.1") }
    );

    await buyInTrx.wait();

    const trx = await this.royaltyItemProxyFactory.CreatorAcceptBuyInOfferMint(
      [
        10,
        ethers.utils.parseUnits(10e8.toPrecision(), "18"),
        ethers.utils.parseUnits((0.008).toPrecision(), "18"),
        ethers.utils.parseUnits(0.012.toPrecision(), "18"),
        "SafaRI",
        "SFRI",
        "logoURL",
        "https://jpg.media.com/?id=20",
        "https://some.media.com/?id=20&format=json",
        "https://jpg.media.com/?id=20",
        "https://jpg.media.com/?id=20",
        "https://app.media.com/view?=20",
        ethers.utils.parseUnits(5e1.toPrecision(), "18"),
        ethers.utils.parseUnits(0e0.toPrecision(), "18"),
        ethers.utils.parseUnits(20e1.toPrecision(), "18"),
        "phil",
        10,
        "offer-123",
      ]
    );

    await trx.wait();

    const itemMgrIndex = await this.royaltyItemProxyFactory.userTokenExists(owner.address, "SafaRI", "SFRI");

    const mgrAddr = await this.royaltyItemProxyFactory.royaltyItemManager(itemMgrIndex[1]);

    const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");

    const itemMgr = await RoyaltyItemManager.attach(mgrAddr);

    expect(await itemMgr.balanceOf(buyer.address)).to.equal(ethers.utils.parseUnits(1e7.toString(), "18"));
    expect(await itemMgr.balanceOf(owner.address)).to.equal(ethers.utils.parseUnits(4e7.toString(), "18"));
  });
});
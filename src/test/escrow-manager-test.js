const { ethers, upgrades } = require("hardhat");
const { expect } = require("chai");

describe("EscrowManager", async function(){

    beforeEach(async function(){
    const Setting = await ethers.getContractFactory("Settings");
    const [owner,addr0] = await ethers.getSigners();
    const setting = await Setting.deploy();
    await setting.deployed();

    const RoyaltyItemManager = await ethers.getContractFactory("RoyaltyItemManager");
    const royaltyItemManager = await RoyaltyItemManager.deploy(setting.address);
    await royaltyItemManager.deployed();

    const RoyaltyERC721 = await ethers.getContractFactory("RoyaltyERC721");
    const royaltyNFT = await RoyaltyERC721.deploy();
    await royaltyNFT.deployed();

    //ESCROW-ADMIN: 0x10E813435B4b54D789E635CC04073d32eF83539C
    const EscrowManager = await ethers.getContractFactory("EscrowManager");
    const escrowMgr = await upgrades.deployProxy(EscrowManager,[addr0.address]);
    await escrowMgr.deployed();
     
    const RoyaltyItemProxyFactory = await ethers.getContractFactory("RoyalItemFactoryProxy");
    this.royaltyItemProxyFactory = await RoyaltyItemProxyFactory.deploy(
        setting.address, royaltyItemManager.address, royaltyNFT.address, setting.address, escrowMgr.address);
    await this.royaltyItemProxyFactory.deployed();
    const trx = await this.royaltyItemProxyFactory.mint(
        [
          10,
          ethers.utils.parseUnits(10e8.toPrecision(), "18"),
          ethers.utils.parseUnits((0.008).toPrecision(), "18"),
          ethers.utils.parseUnits(0.012.toPrecision(), "18"),
          "SafaRI",
          "SFRI",
          "logoURL",
          "https://jpg.media.com/?id=20",
          "https://some.media.com/?id=20&format=json",
          "https://jpg.media.com/?id=20",
          "https://jpg.media.com/?id=20",
          "https://app.media.com/view?=20",
          ethers.utils.parseUnits(5e1.toPrecision(), "18"),
          ethers.utils.parseUnits(0e0.toPrecision(), "18"),
          ethers.utils.parseUnits(0e0.toPrecision(), "18"),
          "phil",
          10,
          "offer-123",
        ]
      );
    this.escMgrAddress = await this.royaltyItemProxyFactory.escrowMgr();
    this.escAdmin = await this.royaltyItemProxyFactory.escrowAdmin();
    });

    it("should deposit escrow funds from minted vault", async function(){
        const [owner] = await ethers.getSigners();
        const EscrowManager = await ethers.getContractFactory("EscrowManager");
        const escrowMgr = await EscrowManager.attach(this.escMgrAddress);
        const fac = this.royaltyItemProxyFactory
        expect(await escrowMgr.balance(await fac.royaltyItemManager(0))).to.equal("0");

        //Disabled escrow
        // expect(await escrowMgr.balance(await fac.royaltyItemManager(0))).to.equal("10000000000000000000000000");
       
        /**
         * trx.erc20Token,
        trx.buyInOffer,
        trx.amount,
        trx.itemId,
        trx.buyer
         */

        const trx = await escrowMgr.transaction("offer-123");

        //Disabled escrow
        // expect(trx[0]).to.equal(await fac.royaltyItemManager(0));
        // expect(trx[1]).to.equal("offer-123");
        // expect(trx[2]).to.equal("10000000000000000000000000");
        // expect(trx[3]).to.equal(10);
        // expect(trx[4]).to.equal("0x0000000000000000000000000000000000000000");
    });

    // it("should release escrow funds from minted vault", async function(){
    //     const [owner] = await ethers.getSigners();
    //     const EscrowManager = await ethers.getContractFactory("EscrowManager");
    //     const escrowMgr = await EscrowManager.attach(this.escMgrAddress);
    //     const fac = this.royaltyItemProxyFactory
    //     expect(await escrowMgr.balance(await fac.royaltyItemManager(0))).to.equal("10000000000000000000000000");
    //     /**
    //      * trx.erc20Token,
    //     trx.buyInOffer,
    //     trx.amount,
    //     trx.itemId,
    //     trx.buyer
    //      */
    //     const trx = await escrowMgr.transaction(20);
    //     expect(trx[0]).to.equal(await fac.royaltyItemManager(0));
    //     expect(trx[1]).to.equal(20);
    //     expect(trx[2]).to.equal("10000000000000000000000000");
    //     expect(trx[3]).to.equal(10);
    //     expect(trx[4]).to.equal("0x0000000000000000000000000000000000000000");
    // });

});
import React from "react";
import { Route } from "react-router-dom";
import Login from "../pages/Auth/login";
import useAuth from "../context/authContext";

export default function Protected({ element, path }) {
  const { isAuthenticated, currentUser } = useAuth();

  if (isAuthenticated && currentUser) {
    return <Route path={path} element={element} />;
  } else {
    return <Route path="/login" element={<Login />} />;
  }
}
